<?php

/*
 *
 *  enqueues for styles and scripts
 */

/* Main start */
require_once(TEMPLATEPATH . '/includes/option-pages.php');
/* Main end */

require_once(TEMPLATEPATH . '/includes/enqueues.php');
require_once(TEMPLATEPATH . '/includes/functions/dashboard-customisation.php');
/*
 * 
 *  setup
 */
if (!function_exists('marcbousleiman_setup')) :

    function marcbousleiman_setup() {
        add_theme_support('post-thumbnails');
        add_theme_support('menus');
//        add_image_size('image_size_450_285', 450, 285, true);
    }

endif;

add_action('after_setup_theme', 'marcbousleiman_setup');

// removing admin bar from all users
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (is_user_logged_in()) {
        show_admin_bar(false);
    }
}

//removing tags from post page
add_action('init', 'remove_tags');

function remove_tags() {
    register_taxonomy('post_tag', array());
}

//function for my custom excerpt
function marc_custom_length($content, $count) {
    if (strlen($content) > $count) {
        $content = substr($content, 0, $count) . '...';
        echo $content;
    } else {
        echo $content;
    }
}

function custom_length_return($content, $count) {
    if (strlen($content) > $count) {
        $count = $count - 22;
//        $content = substr($content, 0, $count) . '...';
        $content = mb_substr($content, 0, $count, 'utf-8') . '...';
        return html_entity_decode($content);
    } else {
        return html_entity_decode($content);
    }
}

/*
 * 
 *  function tochange post page name
 */

//function revcon_change_post_object() {
//    global $wp_post_types;
//    $labels = &$wp_post_types['post']->labels;
//    $labels->name = 'Shops';
//    $labels->singular_name = 'Shop';
//    $labels->add_new = 'Add Shop';
//    $labels->add_new_item = 'Add Shop';
//    $labels->edit_item = 'Edit Shops';
//    $labels->new_item = 'Shops';
//    $labels->view_item = 'View Shops';
//    $labels->search_items = 'Search Shops';
//    $labels->not_found = 'No Shop found';
//    $labels->not_found_in_trash = 'No Shop found in Trash';
//    $labels->all_items = 'All Shops';
//    $labels->menu_name = 'Shops';
//    $labels->name_admin_bar = 'Shops';
//}
//
//add_action('init', 'revcon_change_post_object');