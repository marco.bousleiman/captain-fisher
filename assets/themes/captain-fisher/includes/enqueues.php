<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function marcbousleiman_styles() {
    wp_enqueue_style('style', get_template_directory_uri() . '/assets/styles.css', array(), '1.0');

    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/fonts/font-awesome/css/fontawesome-all.css', array(), '1.0');
}

add_action('wp_enqueue_scripts', 'marcbousleiman_styles');

/*
 * 
 *  javascriptt enqueue
 */

function marcbousleiman_scripts() {
    wp_enqueue_script(
            'script', get_template_directory_uri() . '/assets/scripts.js', array('jquery')
    );
    
}

add_action('wp_enqueue_scripts', 'marcbousleiman_scripts');

