<?php
/**
 * Dashboard customisation
 * 
 */

/**
 * Remove wp update notice
 */
function remove_wp_update_notice() {
    if (!current_user_can('manage_options')) {
        remove_action('admin_notices', 'update_nag', 3);
    }
}

add_action('admin_init', 'remove_wp_update_notice');

/**
 * Remove dashboard footer
 */
if (!function_exists('dashboard_footer')) {

    function dashboard_footer() {
        
    }

}
add_filter('admin_footer_text', 'dashboard_footer');

/**
 * Remove wp logo from top bar
 */
function annointed_admin_bar_remove() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
}

add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);

/**
 * Remove links from admin bar
 */
function remove_admin_bar_links() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
    $wp_admin_bar->remove_menu('updates');          // Remove the updates link
    $wp_admin_bar->remove_menu('comments');         // Remove the comments link
    $wp_admin_bar->remove_menu('new-content');      // Remove the content link
    $wp_admin_bar->remove_menu('w3tc');             // If you use w3 total cache remove the performance link
}

add_action('wp_before_admin_bar_render', 'remove_admin_bar_links');

/**
 * Show default dashboard widgets
 */
function remove_dashboard_widgets() {
    global $wp_meta_boxes;
//    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}

add_action('wp_dashboard_setup', 'remove_dashboard_widgets');

/**
 * Remove screen options
 */
function remove_screen_options($display_boolean, $wp_screen_object) {
    $blacklist = array('post.php', 'post-new.php', 'index.php', 'edit.php');
    if (in_array($GLOBALS['pagenow'], $blacklist)) {
        $wp_screen_object->render_screen_layout();
        $wp_screen_object->render_per_page_options();
        return false;
    } else {
        return true;
    }
}

add_filter('screen_options_show_screen', 'remove_screen_options', 10, 2);

/**
 * Remove tags and comments from columns in edit posts
 */
function my_manage_columns($columns) {
    unset($columns['tags']);
    unset($columns['comments']);
    return $columns;
}

function my_column_init() {
    add_filter('manage_posts_columns', 'my_manage_columns');
}

add_action('admin_init', 'my_column_init');

/**
 * Remove help options
 */
function remove_help_tabs($old_help, $screen_id, $screen) {
    $screen->remove_help_tabs();
    return $old_help;
}

add_filter('contextual_help', 'remove_help_tabs', 999, 3);

/**
 * Remove powered by wp option
 */
function my_footer_shh() {
    remove_filter('update_footer', 'core_update_footer');
}

add_action('admin_menu', 'my_footer_shh');

/**
 * Change custom dashboard login logo
 */
function my_login_logo() {
    ?>
    <style type="text/css">
        body.login, html {
            background: #028482 none repeat scroll 0 0;
        }
        .login #login #backtoblog a, .login #nav a, .login h1 a {
            color: #5e5f61;
        }
        .login #login #backtoblog a:hover, .login #nav a:hover, .login h1 a:hover {
            color: #000;
        }
        .login #login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png);
            padding-bottom: 30px;
            background-size: 200px auto;
            height:100px;
            width: 200px;
        }
        .login .newsociallogins,#loginform h3{
            display:none;
        }
        .login.login-action-login .wp-core-ui .button-primary {
            background: #fff none repeat scroll 0 0 !important;
            border-color: #000;
            box-shadow: 0 1px 0 rgba(1, 124, 61, 0.5) inset, 0 1px 0 rgba(0, 0, 0, 0.15);
        }
        .login.login-action-login .wp-core-ui .button-primary.focus,.login .wp-core-ui .button-primary.hover,.login .wp-core-ui .button-primary:focus,.login .wp-core-ui .button-primary:hover {
            background: #000 none repeat scroll 0 0 important;
            border-color: #000 important;
        }
        #backtoblog{
            display:none!important;
        }
        .login #login .message {
            background-color: #fff;
            border-left: 4px solid #5e5f61;
            box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1);
        }
    </style>
    <?php
}

add_action('login_enqueue_scripts', 'my_login_logo');

/**
 * Set custom dashboard logo url
 */
function custom_loginlogo_url($url) {
    return '/wp-admin';
}

add_filter('login_headerurl', 'custom_loginlogo_url');

/**
 * Remove admin notices (generated by plugins)
 */
function wphidenag() {
    remove_action('admin_notices', 'update_nag', 3);
}

add_action('admin_menu', 'wphidenag');

/**
 * Change dashboard colors
 */
function marcbousleiman_dashboard_styles() {
    echo '<style>
    #adminmenu li.current a.menu-top, #adminmenu li.wp-has-current-submenu .wp-submenu .wp-submenu-head, #adminmenu li.wp-has-current-submenu a.wp-has-current-submenu, .folded #adminmenu li.current.menu-top {
            background: #5e5f61 none repeat scroll 0 0;
        }
        #adminmenu a:hover, #adminmenu li.menu-top:hover, #adminmenu li.opensub > a.menu-top, #adminmenu li > a.menu-top:focus {
    background-color: #000;
}
#adminmenu .wp-has-current-submenu .wp-submenu a:focus, #adminmenu .wp-has-current-submenu .wp-submenu a:hover, #adminmenu .wp-has-current-submenu.opensub .wp-submenu a:focus, #adminmenu .wp-has-current-submenu.opensub .wp-submenu a:hover, #adminmenu .wp-submenu a:focus, #adminmenu .wp-submenu a:hover, #adminmenu a.wp-has-current-submenu:focus + .wp-submenu a:focus, #adminmenu a.wp-has-current-submenu:focus + .wp-submenu a:hover, .folded #adminmenu .wp-has-current-submenu .wp-submenu a:focus, .folded #adminmenu .wp-has-current-submenu .wp-submenu a:hover {
    color: #000;
}
.login .wp-core-ui .button-primary {
    background: #5e5f61 none repeat scroll 0 0 important;
    border-color: #5e5f61 important;
    box-shadow: 0 1px 0 #5e5f61 inset, 0 1px 0 rgba(0, 0, 0, 0.15) important;
}
.login .wp-core-ui .button-primary:focus,.login .wp-core-ui .button-primary:hover {
    background: #000 none repeat scroll 0 0 important;
    border-color: #000 important;
    box-shadow: 0 1px 0 #000 inset important;
}
#adminmenu .awaiting-mod, #adminmenu .update-plugins {
    background: #5e5f61 none repeat scroll 0 0;
}
#wp-admin-bar-view{
display:none!important;
}
.ab-sub-wrapper #wp-admin-bar-user-info .ab-item{
pointer-events: none;
}
.post-type-about #wpbody-content .add-new-h2{
display:none !important; 
}
.acf-button.blue {
    background-color: #5e5f61;
    border-color: #5e5f61;
}
.acf-button.blue:hover,.acf-button.blue:focus {
    background-color: #000;
    border-color: #000;
}
#adminmenu li.menu-top:hover div.wp-menu-image::before, #adminmenu li.opensub > a.menu-top div.wp-menu-image::before {
    color: #fff;
}
#adminmenu .wp-has-current-submenu.opensub .wp-submenu li.current a:focus, #adminmenu .wp-has-current-submenu.opensub .wp-submenu li.current a:hover, #adminmenu .wp-submenu li.current a:focus, #adminmenu .wp-submenu li.current a:hover, #adminmenu a.wp-has-current-submenu:focus + .wp-submenu li.current a:focus, #adminmenu a.wp-has-current-submenu:focus + .wp-submenu li.current a:hover {
    color: #000;
}
a{
color:#5e5f61;
}
a:hover{
color:#000;
}
#screen-options-link-wrap{
display:none;
}
.admin-menu-not {
    background-color: #c90000;
    border: 1px solid #c90000;
    border-radius: 20px;
    color: #fff;
    margin: 0 0 0 4px;
    font-weight: bold;
    padding: 0 6px;
}
.post-state{
            color: red;
        }
.postbox{
border: 1px solid grey;
}
#poststuff .postbox h2 {
    background-color: grey;
    color: #fff;
}
.wp-core-ui .button-primary {
            background: #000 none repeat scroll 0 0;
            border-color: #000;
            box-shadow: 0 1px 0 rgba(1, 124, 61, 0.5) inset, 0 1px 0 rgba(0, 0, 0, 0.15);
             text-shadow: 0 -1px 1px #0000, 1px 0 1px #000, 0 1px 1px #000, -1px 0 1px #000;
        }
.wp-core-ui .button-primary.focus, .wp-core-ui .button-primary.hover, .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover {
            background: #000 none repeat scroll 0 0;
            border-color: #000;
        }
        #adminmenu li.wp-menu-separator{
        background-color: rgba(0, 0, 0, 0.24);
    cursor: inherit;
    height: 5px;
    margin: 10px 0;
    padding: 0;
}
  </style>';
}

add_action('admin_head', 'marcbousleiman_dashboard_styles');

/**
 * Change dashboard colors
 */
global $current_user;
get_currentuserinfo();
if ($current_user->ID == 7) {

    function dashboard_styles_for_non_dev() {
        echo '
<style type="text/css">    
#adminmenuwrap #menu-media,#adminmenuwrap #menu-pages,#adminmenuwrap #menu-comments,#adminmenuwrap #toplevel_page_wpcf7,
#adminmenuwrap #menu-appearance,#adminmenuwrap #menu-plugins,#adminmenuwrap #menu-tools,#adminmenuwrap #menu-settings,
#adminmenuwrap #toplevel_page_edit-post_type-acf-field-group,#adminmenuwrap #toplevel_page_lockdown-wp-admin,
#toplevel_page_CF7DBPluginSubmissions ul ,#adminmenuwrap #toplevel_page_activity_log_page,#adminmenuwrap #toplevel_page_wpseo_dashboard,
#wpadminbar #wp-admin-bar-wpseo-menu{
    display: none;
}
#toplevel_page_CF7DBPluginSubmissions .wp-submenu.wp-submenu-wrap li:nth-child(3),#toplevel_page_CF7DBPluginSubmissions .wp-submenu.wp-submenu-wrap li:nth-child(4) {
    display: none;
}
</style>';
    }

    add_action('admin_head', 'dashboard_styles_for_non_dev');
}



/**
 * Set default admin color scheme
 */
function change_admin_color($result) {
    return 'light';
}

add_filter('get_user_option_admin_color', 'change_admin_color');

/**
 * Show pages and posts IDs
 */
function revealid_add_id_column($columns) {
    $columns['revealid_id'] = 'ID';
    return $columns;
}

add_filter('manage_pages_columns', 'revealid_add_id_column', 5);

function revealid_id_column_content($column, $id) {
    if ('revealid_id' == $column) {
        echo $id;
    }
}

add_action('manage_pages_custom_column', 'revealid_id_column_content', 5, 2);


